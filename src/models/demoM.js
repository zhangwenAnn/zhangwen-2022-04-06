import u from 'updeep';

const STATE = {};

export default {
    namespace: 'demo',

    state: u(STATE, null),

    subscriptions: {
        setup({ dispatch, history }) {
            // todo
        },
    },

    effects: {
        
    },

    reducers: {
        updateState(state, action) {
            return u(action.payload, state);
        },
        resetState(state, action) {
            return u(STATE, null);
        },
    },
};
