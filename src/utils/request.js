import isObject from 'lodash/isObject';
import pickBy from 'lodash/pickBy';
import { message } from 'antd';

/**
 * 请求封装
 * 支持 Get 和 Post 请求
 */
class Http {
    constructor(config) {
        this.defaults = config;
    }
    _request(options) {
        const t = this;
        return new Promise((resolve, reject) => {
            var xhr = new XMLHttpRequest();
            if (options.type == 'get') {
                let paramStrs = Object.keys(options.body).map((item) => {
                    let value = options.body[item];
                    return `${item}=${value || (value == 0 ? value : "")}`;
                });

                let params = [].concat(paramStrs, [`_t=${new Date().valueOf()}`]);
                let urlA = `${options.url}?${params.join("&")}`;
                xhr.responseType = options.dataType;
                xhr.open(options.type, urlA)
                xhr.send();
            } else if (options.type == 'post') {
                xhr.open(options.type, options.url);
                xhr.setRequestHeader('Content-type', options.contentType);
                xhr.responseType = options.dataType;
                xhr.send(options.data)
            }
            xhr.onreadystatechange = function () {
                if (xhr.readyState == 4 && xhr.status == 200) {
                    var response = JSON.parse(xhr.responseText);
                    resolve(response);
                } else {
                    reject(new Error("Request was unsuccessful: " + xhr.statusText));
                }
            }
        })
            .then(response => {
                return t._responseHandler(response);
            })
            .catch(err => {
                t._errorHandler(err);
                return { data: null };
            });
    }
    /**
     * Get 请求
     * @param  {string} url
     * @param  {object} [options]
     * @return {object}
     */
    get(url, options) {
        const { body = {}, extraHeader } = options;
        const { headers, extraQs = {} } = this.defaults;
        const postData = isObject(body)
            ? pickBy(body, function (value) {
                return value !== '' && value !== undefined && value !== null;
            })
            : {};

        const config = {
            url,
            type: 'get',
            data: { ...extraQs, ...postData },
            dataType: 'json'
        };
        return this._request(config);
    }

    /**
     * Post 请求
     * @param {string} url
     * @param {object} options
     */
    post(url, options) {
        const { body, extraHeader } = options;
        const config = {
            url,
            data: body,
            type: 'post',
            dataType: 'json',
            contentType: `${isObject(body) ? 'application/x-www-form-urlencoded;' : 'application/json;'
                }charset=UTF-8`,
        };
        return this._request(config);
    }
    /**
     * 响应处理
     * @param {*} res
     */
    _responseHandler(res) {
        if (res) {
            if (res.result === 0) {
                return res;
            } else {
                message.error(res.msg || '请求信息错误');
            }
        } else {
            message.error('请求数据失败，请刷新重试！');
        }
        return false;
    }

    /**
     * 请求错误处理
     * @param {*} err
     */
    _errorHandler(err) {
        if (err) {
            let msg;
            switch (err.status) {
                case 404:
                    msg = '接口请求不存在！错误码【404】。';
                    break;
                case 500:
                    msg = '服务端应用接口异常！错误码【500】。';
                    break;
                default:
                    msg = '请求错误，请检查或刷新重试！';
                    break;
            }
            message.error(msg);
        }
    }
}
const http = new Http({
    // 全局变量，记录操作信息，在 public/index.ejs 定义
    OPERATE_INFO: 'OPERATE_INFO' in window ? OPERATE_INFO : ''
});

export default http;
