import React ,{useEffect,useState} from 'react';
import { connect } from 'dva';
import styles from './index.less';

function Ipad() {
    const [activeTab,setActiveTab] = useState('CHARACTERS')
    return (
        <div className={styles.normal} >
            <div className={styles.headerWraper}>
                <img src={'./resources/images/logo.png'}/>
            </div>
            <div className={styles.yqWraper}>
                <div className={styles.hxWraper}></div>
                <div className={styles.sixWrapper}>
                    <div className={styles.leftSjx}></div>
                    <div className={styles.centerCnt}>{activeTab}</div>
                    <div className={styles.rightSzx}></div>
                </div>
            </div>
            <div className={styles.contentWrapper}>
                <div className={styles.imgWraper}>
                    <img src={'./resources/images/img-1.jpg'}/>
                </div>
                <div className={styles.imgWraper}>
                    <img src={'./resources/images/img-1.jpg'}/>
                </div>
                <div className={styles.imgWraper}>
                    <img src={'./resources/images/img-1.jpg'}/>
                </div>
                <div className={styles.imgWraper}>
                    <img src={'./resources/images/img-1.jpg'}/>
                </div>
                <div className={styles.imgWraper}>
                    <img src={'./resources/images/img-1.jpg'}/>
                </div>
                <div className={styles.imgWraper2}>
                    <img src={'./resources/images/img-1.jpg'}/>
                    <img src={'./resources/images/img-1.jpg'}/>
                    <img src={'./resources/images/img-1.jpg'}/>
                    <img src={'./resources/images/img-1.jpg'}/>
                </div>
            </div>
        </div>
    );
}

export default connect(({ example, loading }) => ({
    example,
    loading,
}))(Ipad);
