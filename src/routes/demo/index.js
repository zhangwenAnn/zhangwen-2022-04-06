import React ,{useEffect,useState} from 'react';
import { connect } from 'dva';
import styles from './index.less';
import Pc from './Pc'
import Mobile from './Mobile'
import Ipad from './Ipad'
function Demo() {
    const [width,setWidth]=useState(0)
    useEffect(()=>{
        handleResize();
        window.addEventListener('resize',handleResize);
        return ()=>{
            window.removeEventListener('resize',handleResize)
        }
    },[])
    function handleResize(){
        let width=window.innerWidth||document.documentElement.clientWidth||document.body.clientWidth;
        setWidth(width)
    }
    return (
        <div style={{width:'100%',height:'100%'}}>
            {width>1000&&<Pc />}
            {(width<639||width==639)&&<Mobile />}
            {(width<1000&&(width>640||width==640))&&<Ipad />}
        </div>       
    );
}

export default connect(({ example, loading }) => ({
    example,
    loading,
}))(Demo);
