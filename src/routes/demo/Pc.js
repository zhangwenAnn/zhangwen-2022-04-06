import React ,{useEffect,useState} from 'react';
import { connect } from 'dva';
import styles from './index.less';

function Pc() {
    const [activeTab,setActiveTab] = useState('CHARACTERS')
    return (
        <div className={styles.normal}>
            <div className={styles.header}>
                <div className={styles.tabWraper}>
                    <div className={styles.borderleft}></div>
                    <div className={styles.headerCenter}>
                        <div className={styles.headerTab}>
                            <div className={styles.tabLi} onClick={()=>{
                                setActiveTab('HOME')
                            }} style={{color: activeTab=='HOME'?'rgb(209, 106, 32)':'#fff'}}>HOME</div>
                            <div className={styles.tabLi} onClick={()=>{
                                setActiveTab('NEWS')
                            }} style={{color: activeTab=='NEWS'?'rgb(209, 106, 32)':'#fff'}}>NEWS</div>
                            <div className={styles.tabLi} onClick={()=>{
                                setActiveTab('WEARPONS')
                            }} style={{color: activeTab=='WEARPONS'?'rgb(209, 106, 32)':'#fff'}}>WEARPONS</div>
                            <div className={styles.tabLi} onClick={()=>{
                                setActiveTab('MAP')
                            }} style={{color: activeTab=='MAP'?'rgb(209, 106, 32)':'#fff'}}>MAP</div>
                            <div className={styles.tabLi}  onClick={()=>{
                                setActiveTab('CHARACTERS')
                            }} style={{color: activeTab=='CHARACTERS'?'rgb(209, 106, 32)':'#fff'}}>CHARACTERS</div>
                            <div className={styles.tabLi} onClick={()=>{
                                setActiveTab('WALLPAPER')
                            }} style={{color: activeTab=='WALLPAPER'?'rgb(209, 106, 32)':'#fff'}}>WALLPAPER</div>
                        </div>                       
                    </div>
                    <div className={styles.borderRight}></div>
                </div>
                <div className={styles.logoWrapper}>
                    <img src={'./resources/images/logo.png'}/>
                </div>
            </div>
            <div className={styles.yqWraper}>
                <div className={styles.hxWraper}></div>
                <div className={styles.sixWrapper}>
                    <div className={styles.leftSjx}></div>
                    <div className={styles.centerCnt}>{activeTab}</div>
                    <div className={styles.rightSzx}></div>
                </div>
            </div>
            <div className={styles.contentWrapper}>
                <div className={styles.imgWraper}>
                    <img src={'./resources/images/img-1.jpg'}/>
                </div>
                <div className={styles.imgWraper}>
                    <img src={'./resources/images/img-1.jpg'}/>
                </div>
                <div className={styles.imgWraper}>
                    <img src={'./resources/images/img-1.jpg'}/>
                </div>
                <div className={styles.imgWraper}>
                    <img src={'./resources/images/img-1.jpg'}/>
                </div>
                <div className={styles.imgWraper}>
                    <img src={'./resources/images/img-1.jpg'}/>
                </div>
                <div className={styles.imgWraper2}>
                    <img src={'./resources/images/img-1.jpg'}/>
                    <img src={'./resources/images/img-1.jpg'}/>
                    <img src={'./resources/images/img-1.jpg'}/>
                    <img src={'./resources/images/img-1.jpg'}/>
                </div>
            </div>
        </div>
    );
}

export default connect(({ example, loading }) => ({
    example,
    loading,
}))(Pc);
